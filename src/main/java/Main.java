import controller.ConsoleController;
import model.Model;
import model.WordWrapper;
import view.ConsoleView;
import view.View;

public class Main {

    public static void main(String[] args) {

        View view = new ConsoleView();

        Model model = new WordWrapper();

        ConsoleController controller = new ConsoleController(view, model);

        controller.run();
    }

}