package view;

import model.WordWrapper;

import java.util.List;

public class ConsoleView implements View {

    public void print(List<WordWrapper> stringList) {

        for (WordWrapper s: stringList) {
            System.out.println(s.getWord());
        }
    }

}