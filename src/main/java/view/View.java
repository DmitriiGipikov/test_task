package view;

import model.WordWrapper;

import java.util.List;

public interface View {

    void print(List<WordWrapper> stringList);

}
