package controller;

import model.Model;
import model.WordWrapper;
import view.View;

import java.util.*;

public class ConsoleController implements Controller {

    private View view;
    private Model model;

    public ConsoleController(View view, Model model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void run() {
        List<String> words = scanConsole();
        List<WordWrapper> wordWrappers = model.wrapWords(words);
        sort(wordWrappers);
        view.print(wordWrappers);
    }

    private List<String> scanConsole() {
        int count = new Scanner(System.in).nextInt();
        String words = new Scanner(System.in).nextLine();

        return split(words);
    }

    private List<String> split(String words) {
        return Arrays.asList(words.split(" "));
    }

    private void sort(List<WordWrapper> words) {
        Comparator<WordWrapper> comparator = (o1, o2) -> {
            int result = 0;
            int numVowelsFirstWord = o1.getNumVowels();
            int numVowelsSecondWord = o2.getNumVowels();
            int lengthFirstWord = o1.getLength();
            int lengthSecondWord = o2.getLength();

            if (numVowelsFirstWord > numVowelsSecondWord) {
                result = 1;
            }
            else if (numVowelsFirstWord < numVowelsSecondWord) {
                result = -1;
            }
            else {
                if (lengthFirstWord > lengthSecondWord) {
                    result = 1;
                }
                else if (lengthFirstWord < lengthSecondWord) {
                    result = -1;
                }
                else {
                    result = o1.getPosition() - o2.getPosition();
                }
            }
            return result;
        };
        words.sort(comparator.reversed());
    }

}