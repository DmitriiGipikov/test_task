package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordWrapper implements Model {

    private int position;
    private int numVowels;
    private int length;
    private String word;
    private static final List<Character> vowels = new ArrayList<>(Arrays.asList('А', 'Е', 'Ё', 'И', 'О', 'У', 'Э', 'Ю', 'Ы', 'Я', 'а', 'е', 'ё', 'и', 'о', 'у', 'э', 'ю', 'ы', 'я'));

    public WordWrapper() {
    }

    public WordWrapper(int position, int numVowels, int length, String word) {
        this.position = position;
        this.numVowels = numVowels;
        this.length = length;
        this.word = word;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getNumVowels() {
        return numVowels;
    }

    public void setNumVowels(int numVowels) {
        this.numVowels = numVowels;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    private int numVowel(String string) {
        int count = 0;

        for (int i = 0; i < string.length(); i++) {
            if (vowels.contains(string.charAt(i))) {
                count += 1;
            } else {
                count += 0;
            }
        }
        return count;
    }

    public List<WordWrapper> wrapWords(List<String> words) {
        List<WordWrapper> wrappers = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            WordWrapper wrapper = new WordWrapper();
            String word = words.get(i);
            wrapper.setLength(word.length());
            wrapper.setPosition(i);
            wrapper.setWord(word);
            wrapper.setNumVowels(numVowel(word));
            wrappers.add(wrapper);
        }
        return wrappers;
    }

}