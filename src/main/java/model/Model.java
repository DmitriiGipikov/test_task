package model;

import java.util.List;

public interface Model {

    List<WordWrapper> wrapWords(List<String> words);

}
